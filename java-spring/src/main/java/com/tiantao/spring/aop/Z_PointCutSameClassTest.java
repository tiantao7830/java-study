package com.tiantao.spring.aop;

import java.util.Map;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;

import com.tiantao.common.utils.CollectionsUtils;

/**
 * 同类调用同类，aop不起作用的处理方案
 * @author tiantao
 *
 */
@Component
public class Z_PointCutSameClassTest {
	/**
	 * 创建两个参数的方法
	 * @param a1
	 * @param a2
	 */
	public void testParams2(String a1,String a2){
		//1. 获取当前类的代理对象
		Z_PointCutSameClassTest proxyClazz = (Z_PointCutSameClassTest)AopContext.currentProxy();
		
		//2. 使用代理类调用当前方法
		proxyClazz.testMap(CollectionsUtils.createHashMap(a1,a2));
	}
	
	/**
	 * 创建传入map参数的方法
	 * @param map
	 */
	public void testMap(Map<String,Object> map){
		
	}

}
