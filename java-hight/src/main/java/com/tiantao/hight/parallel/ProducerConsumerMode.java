package com.tiantao.hight.parallel;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 生产者，消费者模式
 * @author tiantao
 *
 */
public class ProducerConsumerMode {
	public static void main(String[] args) {
		//1. 创建任务队列（该队列如果满了，会造成线程阻塞，知道队列不满，那么再插入数据）
		BlockingQueue<String> queue = new LinkedBlockingQueue<String>(5);
		
		//2. 创建生成者，生产数据
		Producer producer1 = new Producer("producer1", queue);
		Producer producer2 = new Producer("producer2", queue);
		Producer producer3 = new Producer("producer3", queue);
		
		//3. 创建消费者
		Consumer consumer1 = new Consumer("consumer1", queue);
		Consumer consumer2 = new Consumer("consumer2", queue);
		Consumer consumer3 = new Consumer("consumer3", queue);
		
		//4. 启动生产者，消费者
		ExecutorService service = Executors.newCachedThreadPool();
		service.execute(producer1);
		service.execute(producer2);
		service.execute(producer3);
		service.execute(consumer1);
		service.execute(consumer2);
		service.execute(consumer3);
		
	}
}
/**
 * 生产者
 */
class Producer extends Thread{
	//任务队列，与消费者中的任务队列，指向同一个引用
	private BlockingQueue<String> queue;	
	/**
	 * @param name	线程名称
	 * @param queue	任务队列
	 */
	public Producer(String name,BlockingQueue<String> queue) {
		super.setName(name);
		this.queue = queue;
	}
	public void run() {
		try {
			//生产数据
			for (int i = 0; i < 10; i++) {
				queue.put(String.valueOf(i));
				System.out.println(Thread.currentThread().getName()+"生产者生产数据："+i);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
/**
 * 消费者
 */
class Consumer extends Thread{
	//任务队列，与生产者中的任务队列，指向同一个引用
	private BlockingQueue<String> queue;
	/**
	 * @param name	线程名称
	 * @param queue	任务队列
	 */
	public Consumer(String name,BlockingQueue<String> queue) {
		super.setName(name);
		this.queue = queue;
	}
	@Override
	public void run() {
		try {
			//消费数据
			while(true){
				//1. 获取要消费的数据
				String take = queue.take();
				
				//2. 消费该数据
				System.out.println(Thread.currentThread().getName()+"消费数据:"+take);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}








