package com.tiantao.hight.parallel;

/**
 * 使用future模式
 * 	使用场景：如果一方法运行耗时很长，那么我们可以先返回null，等执行完成之后，我们再返回这方法的返回值
 * @author tiantao
 *
 */
public class FutureMode {
	public static void main(String[] args) {
		Client client = new Client();
		Data data = client.request("tiantao");
		System.out.println("请求结束:"+data);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("最后获取的数据："+data);
	}
	
	
}

interface Data{
	public String getResult();
}

class RealData implements Data{
	protected final String result;
	public RealData(String para) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 10; i++) {
			sb.append(para);
		}
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.result = sb.toString();
	}

	public String getResult() {
		return result;
	}
}


class FutureData implements Data{
	protected RealData realData = null;
	protected boolean isReady = false;

	public synchronized void setRealData(RealData realData) {
		if(isReady){
			return ;
		}
		this.realData = realData;
		isReady = true;
		notifyAll();
	}
	
	public synchronized RealData getRealData() {
		return realData;
	}
	
	public String getResult() {
		while(!isReady){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return realData.getResult();
	}

	@Override
	public String toString() {
		return "FutureData [realData=" + realData + ", isReady=" + isReady + "]";
	}	
}

class Client{
	public Data request(final String queryStr){
		final FutureData future = new FutureData();
		new Thread(){
			@Override
			public void run() {
				RealData realData = new RealData(queryStr);
				future.setRealData(realData);
			}
		}.start();
		return future;
	}
}
