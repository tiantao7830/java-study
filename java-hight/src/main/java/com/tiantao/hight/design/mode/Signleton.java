package com.tiantao.hight.design.mode;

/**
 * 单例模式
 * @author tiantao
 *
 */
public class Signleton {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		//1. 懒汉模式，线程不安全，能懒加载
		Entity entity1 = SignletonIdler1.getEntity();
		//2. 懒汉模式，线程安全，能懒加载,效率低
		Entity entity2 = SignletonIdler2.getEntity();
		//3. 双重校验锁,第二种的升级版，但还是效率低
		Entity entity3 = SignleonDual.getEntity();
		//4. 饿汉模式，不能懒加载，线程安全
		Entity entity4 = SingletonHungry1.getEntity();
		//5. 饿汉模式另一种实现，不能懒加载，线程安全
		Entity entity5 = SingletonHungry2.getEntity();
		//6. 静态内部类，不能懒加载，线程安全
		Entity entity6 = SignletonStatic.getEntity();
		//7. 使用枚举类,作者Josh Bloch提倡,线程安全，能防止反序列化重新创建对象的问题
		Entity entity7 = SignletonEnum.entity1.getEntity();
	}
}

/**
 * 1. 懒汉模式，线程不安全
 * 	特点：1.在类加载时不初始化
 * 		 2.当使用多线程的时候，不能够工作
 * @author tiantao
 */
class SignletonIdler1{
	private static Entity entity;
	public static Entity getEntity(){
		if(entity == null){
			entity = new Entity();
		}
		return entity;
	}
}

/**
 * 2. 懒汉模式，线程安全
 * 	特点：1. 在类加载时不初始化
 * 		 2. 使用锁，也由于锁的原因，效率很低
 * @author tiantao
 *
 */
class SignletonIdler2{
	private static Entity entity;
	public static synchronized Entity getEntity(){
		if(entity == null){
			entity = new Entity();
		}
		return entity;
	}
}

/**
 * 3. 双重校验锁
 * 	第二种的升级版
 * @author tiantao
 *
 */
class SignleonDual{
	private volatile static Entity entity;
	public static Entity getEntity(){
		if(entity == null){
			synchronized (Entity.class){
				if(entity == null){
					entity = new Entity();
				}
			}
		}
		return entity;
	}
}

/**
 * 4. 饿汉模式
 *  特点：1. 在类加载时初始化
 *  	 2. 线程安全
 * @author tiantao
 *
 */
class SingletonHungry1{
	private static Entity entity = new Entity();
	public static Entity getEntity(){
		return entity;
	}
}

/**
 * 5. 饿汉模式，变种
 *  特点：1. 在类加载时初始化
 *  	 2. 线程安全
 * @author tiantao
 *
 */
class SingletonHungry2{
	private static Entity entity = null;
	static{
		entity = new Entity();
	}
	public static Entity getEntity(){
		return entity;
	}
}

/**
 * 6. 静态内部类
 *  特点：1. 在类加载时初始化
 *  	 2. 线程安全
 * @author tiantao
 *
 */
class SignletonStatic{
	private static class SignletonHolder{
		private static final Entity entity = new Entity();
	}
	public static final Entity getEntity(){
		return SignletonHolder.entity;
	}
}

/**
 * 7.使用枚举类
 * 	是Effective Java作者Josh Bloch提倡的方式
 * 	他可以避免多线程同步问题，而且还能防止反序列化重新创建对象的问题，推荐使用
 * @author tiantao
 *
 */
enum SignletonEnum{
	entity1(new Entity());
	
	private Entity entity;
	private SignletonEnum(Entity entity) {
		this.entity = entity;
	}
	public Entity getEntity() {
		return entity;
	}
}




class Entity{
	private String name;
	private Integer age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
}






