package com.tiantao.hight.design.mode;

import java.util.ArrayList;
import java.util.List;

/**
 * 观察者模式
 * 		观察者模式，又可以称之为发布-订阅模式，（观察者的消息是有被观察者报告给观察者的）
 * 		功能场景：微博定于，当我们订阅了某个人的微博账号，当这个人发布了新的消息，就会通知我们
 * @author tiantao
 *
 */
public class Observer {
	public static void main(String[] args) {
		/**
		 * 示例：
		 * 	警察在找到嫌疑犯的时候，为了找到幕后主使，一般都会蹲点监察，这里我有三名便衣警察来蹲点监察2名嫌犯，
		 * 	三名便衣分别是：张昊天、石破天、赵日天 ，两名嫌犯是：大熊，黑狗
		 */
		//1. 定义两个嫌犯
		Huairen xf1 = new XianFan("大熊");
		Huairen xf2 = new XianFan("黑狗");
		//2. 定义三个观察便衣警察
		IObserver o1 = new BianYiPolice("张昊天");
		IObserver o2 = new BianYiPolice("石破天");
		IObserver o3 = new BianYiPolice("赵日天 ");
		//3. 为嫌疑增加观察警察
		xf1.addObserver(o1);
		xf1.addObserver(o2);
		xf2.addObserver(o1);
		xf2.addObserver(o3);
		//4. 定义嫌疑犯的情况
		xf1.notice("又卖了一批货");
		xf2.notice("老大要下来视察了");
	}
}

/**
 * 观察者接口
 * @author tiantao
 *
 */
interface IObserver{
	void update(String message,String name);
}
/**
 * 便衣警察类（观察者的实现类）
 * @author tiantao
 */
class BianYiPolice implements IObserver{
	private String bname;
	public BianYiPolice(String bname) {
		super();
		this.bname = bname;
	}
	public void update(String message, String name) {
		System.out.println(bname + ":" +name+"那边有情况："+message);
	}
}
/**
 * 嫌疑犯接口（被观察的接口）
 * @author tiantao
 */
interface Huairen{
	//添加便衣观察者
	void addObserver(IObserver observer);
	//移除便衣观察者
	void removeObserver(IObserver observer);
	//通知观察者
	void notice(String message);
}

/**
 * 嫌疑犯（被观察的类）
 * @author tiantao
 */
class XianFan implements Huairen{
	private String name;		//嫌疑犯名称
	private List<IObserver> observerList = new ArrayList<IObserver>();	//观察者集合
	public XianFan(String name) {
		this.name = name;
	}
	
	//增加观察者
	public void addObserver(IObserver observer) {
		if(!observerList.contains(observer)){
			observerList.add(observer);
		}
	}
	//移除观察者
	public void removeObserver(IObserver observer) {
		if(observerList.contains(observer)){
			observerList.remove(observer);
		}
	}
	//通知观察者
	public void notice(String message) {
		for (IObserver observer : observerList) {
			observer.update(message, name);
		}
	}
}












