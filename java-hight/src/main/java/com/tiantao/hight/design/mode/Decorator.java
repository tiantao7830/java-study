package com.tiantao.hight.design.mode;

/**
 * 装饰者模式
 * 	核心思想：动态添加对象的功能（使用继承的方式也能动态扩展功能，但是对性能可能会有所影响）
 * 
 * @author tiantao
 */
public class Decorator {
	
	public static void main(String[] args) {
		/**
		 * 案例介绍：
		 * 	公司门口有一个小摊卖手抓饼和肉夹馍的，有时候中午不想吃饭就会去光顾一下那个小摊，
		 * 	点了手抓饼之后往往还可以在这个基础之上增加一些配料，例如煎蛋，火腿片等等，每个配料的价格都不一样，
		 * 	不管你怎么配配料，最终价格是手抓饼基础价加上每一种所选配料价格的总和。
		 * 	小摊的价格单如下：
		 * 		主题		价格		配料		价格
		 * 		手抓饼	4		煎蛋		2
		 * 		肉夹馍	6		火腿片	1.5
		 */
		
		//1. 创建一个肉夹馍类
		Theme rouJiamo = new RouJiamo();
		//2. 加一个鸡蛋
		rouJiamo = new FiredEgg(rouJiamo);
		//3. 加一个火腿片
		rouJiamo = new Ham(rouJiamo);
		
		//4. 查询肉夹馍信息
		System.out.println(rouJiamo.getDesc());
		System.out.println("价格："+rouJiamo.price());
	}
}

/**
 * 主题 (是肉夹馍和煎饼的父类)
 * @author tiantao
 *
 */
abstract class Theme{
	public String desc = "";
	public String getDesc() {
		return desc;
	}
	public abstract double price();
}
/**
 * 手抓饼（被装饰的类）
 * @author tiantao
 */
class TornCake extends Theme{
	public TornCake() {
		desc = "手抓饼";
	}
	@Override
	public double price() {
		return 4;
	}
}
/**
 * 肉夹馍（被装饰的类）
 * @author tiantao
 */
class RouJiamo extends Theme{
	public RouJiamo() {
        desc = "肉夹馍";
    }
    @Override
    public double price() {
        return 6;
    }
}

/**
 * 装饰类的父类
 * @author tiantao
 */
abstract class Condiment extends Theme {
    public abstract String getDesc();
}

/**
 * 煎蛋类（装饰类）
 */
class FiredEgg extends Condiment{
	private Theme theme;		//要装饰的对象
    public FiredEgg(Theme theme) {
        this.theme = theme;
    }
	@Override
	public String getDesc() {
		return theme.getDesc() +",煎蛋";
	}
	@Override
	public double price() {
		return theme.price() + 2;
	}
}

/**
 * 火腿片（装饰类）
 * @author tiantao
 */
class Ham extends Condiment{
	private Theme theme;		//要装饰的对象
    public Ham(Theme theme) {
        this.theme = theme;
    }
	@Override
	public String getDesc() {
		return theme.getDesc() +",火腿片";
	}
	@Override
	public double price() {
		return theme.price() + 1.5;
	}
}
























