package com.tiantao.hight.design.mode;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * 代理模式
 * 	介绍：通过代理对象访问目标对象，
 * 	好处：可以在目标对象实现的基础上，增强额外的功能操作，即扩展目标对象的功能
 * 	使用场景：不要随意去修改别人已经写好的代码或者方法，如果需修改，可以通过代理的方式来扩展该方法
 * @author tiantao
 */
public class Angency {
	
	public static void main(String[] args) {
		IDBQuery query = new DBQuery();						//创建需要代理的类
		
		//1. 静态代理
		System.out.println("\n-----使用静态代理，代理request方法-----");
		StaticProxy staticProxy = new StaticProxy(query);	//创建代理对象
		staticProxy.request();			//执行代理的方法
		
		//2. 使用jdk的方式实现动态代理
		// 与cglib相比的优点：创建代理对象的速度快
		System.out.println("\n-----使用动态代理JDK方式，代理request方法-----");
		IDBQuery jdkInstance = JDKProxy.getProxyInstance();	//创建代理对象
		jdkInstance.request();
		
		//3. 使用cglib的方式实现动态代理
		// 与jdk方式相比的优点：执行速度稍微快一点，（一般用这种方式）
		System.out.println("\n-----使用动态代理cglib方式，代理request方法-----");
		IDBQuery cglibInstance = CglibProxy.getProxyInstance();
		cglibInstance.request();
	}	
}

/**
 * 1. 动态代理的接口
 * @author tiantao
 *
 */
interface IDBQuery{ 
	public void request(); 
}
/**
 * 2. 需要动态代理的类
 * @author tiantao
 */
class DBQuery implements IDBQuery{
	public void request() {
		System.out.println("执行需要代理的类");
	}
}
/**
 * 3. 使用静态代理，代理DBQuery类的request方法
 * @author tiantao
 */
class StaticProxy implements IDBQuery {
	private IDBQuery dbQuery;
	public StaticProxy(IDBQuery dbQuery) {
		this.dbQuery = dbQuery;
	}
	public void request() {
		System.out.println("执行DBQuery.request方法前执行！");
		dbQuery.request();
		System.out.println("执行DBQuery.request方法后执行！");
	}
}

/**
 * 4. 使用jdk的方式动态代理DBQuery类的request方法
 * @author tiantao
 */
class JDKProxy implements InvocationHandler{
	private IDBQuery dbQuery;
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("执行DBQuery.request方法前执行！");
		//使用反射的方式，执行代理的类
		if(dbQuery == null){
			dbQuery = new DBQuery();
		}
		method.invoke(dbQuery, args);
		System.out.println("执行DBQuery.request方法后执行！");
		return null;
	}
	/**
	 * 体统创建该代理对象的方法
	 * @return
	 */
	public static IDBQuery getProxyInstance(){
		return (IDBQuery)Proxy.newProxyInstance(
						ClassLoader.getSystemClassLoader(),
						new Class[]{IDBQuery.class},
						new JDKProxy());
	}
}

/**
 * 5. 使用cglib的方式代理DBQuery类的request方法
 * @author tiantao
 */
class CglibProxy implements MethodInterceptor{
	private IDBQuery dbQuery;
	public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
		System.out.println("执行DBQuery.request方法前执行！");
		//使用反射的方式，执行代理的类
		if(dbQuery == null){
			dbQuery = new DBQuery();
		}
		method.invoke(dbQuery, args);
		System.out.println("执行DBQuery.request方法后执行！");
		return null;
	}
	/**
	 * 提供创建该代理对象的方法
	 * @return
	 */
	public static IDBQuery getProxyInstance(){
		Enhancer enhancer = new Enhancer();
		enhancer.setCallback(new CglibProxy());	//代理类
		enhancer.setInterfaces(new Class[]{IDBQuery.class});
		return (IDBQuery) enhancer.create();
	}
}



