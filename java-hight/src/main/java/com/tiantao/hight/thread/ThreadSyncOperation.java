package com.tiantao.hight.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程同步
 * 	1. 同步方法，使用synchronized修饰的静态方法
 * 		  当调用同步方法的时候，将会锁住整个类
 * 	2. 同步块（能使用同步块的，尽量不要使用同步方法）
 * 		  只会锁住某个变量（由于同步是一种高开销的操作，因此应该尽量减少同步的内容）
 * 		 
 * @author tiantao
 *
 */
public class ThreadSyncOperation {
	public static void main(String[] args) {
		//1. 创建线程池
		ExecutorService threadPool = Executors.newCachedThreadPool();
		
		//2. 执行该线程的内容
		for (int i = 0; i < 20; i++) {
			final Random random = new Random();
			threadPool.execute(new Runnable() {
				public void run() {
					//2.1 普通方法，会出现线程问题
//					ThreadSyncOperation.common();
					//2.2 使用同步方法，线程安全，效率较低
//					ThreadSyncOperation.syncMethod();
					//2.3 使用同步块，比同步方法效率高，线程安全
//					ThreadSyncOperation.syncBlock();
					System.out.println(random.nextInt(4));
				}
			});
		}
		
		//3. 输出最后运行结果
		System.out.println(list);
	}
	
	private static List<Integer> list = new ArrayList<Integer>();		//要求里面存储不重复的数据
	
	/**
	 * 普通方法
	 * 	   启动20个线程同时运行该方法，运行结果是：[null, null, 0, 1, 1, 1, 2, 3, 4]
	 */
	public static void common(){
		for (int i = 0; i < 5; i++) {
			if(!list.contains(i)){
				list.add(i);
			}
		}
	}
	
	/**
	 * 同步方法
	 * 	   启动20个线程同时运行该方法，运行结果是：[0, 1, 2, 3, 4]
	 */
	public static synchronized void syncMethod(){
		for (int i = 0; i < 5; i++) {
			if(!list.contains(i)){
				list.add(i);
			}
		}
	}
	
	/**
	 * 同步块
	 */
	public static void syncBlock(){
		for (int i = 0; i < 5; i++) {
			synchronized (list) {
				if(!list.contains(i)){
					list.add(i);
				}
			}
		}
	}
	
}




