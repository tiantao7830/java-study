package com.tiantao.hight.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 一个针对方法的自定义注解
 * 定义注解步骤：
 * 	1. 注解类需要使用@interface进行修饰
 * 	2. 设置该注解类的运行周期
 * 		@Retention(RetentionPolicy.SOURCE)   // 注解仅存在于源码中，在class字节码文件中不包含
		@Retention(RetentionPolicy.CLASS)     // 默认的保留策略，注解会在class字节码文件中存在，但运行时无法获得
		@Retention(RetentionPolicy.RUNTIME)  // 注解会在class字节码文件中存在，在运行时可以通过反射获取到
	3. 设置该注解的类型
		@Target(ElementType.TYPE)           // 接口、类、枚举、注解
		@Target(ElementType.FIELD)          // 字段、枚举的常量
		@Target(ElementType.METHOD)         // 方法
		@Target(ElementType.PARAMETER)      // 方法参数
		@Target(ElementType.CONSTRUCTOR)    // 构造函数
		@Target(ElementType.LOCAL_VARIABLE) // 局部变量
		@Target(ElementType.ANNOTATION_TYPE)// 注解的注解
		@Target(ElementType.PACKAGE)        // 包
	4. 在一个方法，类，属性，方法参数。。。前使用该注解
 * 
 * @author tiantao
 *
 */
public class MethodAnnotation {
	

	public static void main(String[] args) {
		try {
			new MethodAnnotation().useAnnotation();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 使用反射机制，调用注解的方法，并打印结果
	 * @throws InstantiationException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	public void useAnnotation() throws Exception{
		//1. 获取当前类
		Class<?> clazz = MethodAnnotation.class;
		
		//2. 获取该类中所有方法
		Method[] methods = clazz.getMethods();
		
		//3. 寻找该类中有@PrintResult注解的方法
		for (Method method : methods) {
			if(method.isAnnotationPresent(PrintResult.class)){	//如果该方法有注解PrintResult
				Object result = method.invoke(clazz.newInstance());	//调用该方法，并记录返回值
				System.out.println(result.toString());	//记录返回值
			}
		}
	}
	
	/**
	 * 该方法使用自动调用并打印结果的注解
	 * @return
	 */
	@PrintResult("tests")
	public String tests(){
		return "bbbb";
	}
	
}

/**
 * 使用注解,调用该方法，并打印该方法的返回值
 * @author tiantao
 *
 */
@Retention(RetentionPolicy.RUNTIME)		//设置该注解始终不会丢弃，运行期也保留该注解，因此可以使用反射机制读取该注解信息(自定义注解常用)
@Target({ElementType.METHOD})			//指定该注解是一个方法注解
@interface PrintResult{
	String value();				//如果不指定属性名，直接设置到该值上面,并且该参数必传
	String desc() default "";	//设置可选传入的参数，如果没有传入该参数，默认为""
	
}
