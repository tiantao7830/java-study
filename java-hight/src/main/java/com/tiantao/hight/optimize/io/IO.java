package com.tiantao.hight.optimize.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 缓冲区
 * 	（文件的读取与写入的时候，使用缓冲区，可以提高读写效率）
 * @author tiantao
 *
 */
public class IO {
	
	public static void main(String[] args) {
		new IO().copy1();
	}
	
	
	/**
	 * 使用字符缓冲区读写内容
	 * 	(图片，二进制，文本文件都可以使用)
	 * @return
	 */
	public void copy1(){
		Long begin = System.currentTimeMillis();
		
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			//实例化高效缓冲区字节流的输入，输出对象
			inputStream = new BufferedInputStream(new FileInputStream("C:\\Users\\tiantao\\Pictures\\Saved Pictures\\1.jpg"));
			outputStream = new BufferedOutputStream(new FileOutputStream("E:\\wuyong\\删除\\1.jpg"));
			//每次读取的文件字节数量
			int len = -1;
			//每次读取的文件字节数据
			byte[] bs = new byte[1024];
			//循环读取文件，直到文件末尾
			while((len = inputStream.read(bs)) != -1){
				//写入到文件
				outputStream.write(bs,0,len);
			}
			//清空缓冲区，将写入到文件中的数据保存
			outputStream.flush();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("总耗时："+(System.currentTimeMillis() - begin));
	}
	
}
