package com.tiantao.hight.optimize.collections;

import java.util.HashMap;
import java.util.Map;

/**
 * 1. 重写HashCode和equals
 * 	  使用场景：
 * 		假设创建了两个对象：
 * 			 User user1=new User(18,"周杰伦")；
 * 			 User user2=new User(18,"周杰伦")；
 * 		将这两个对象作为一个Key存储到HashMap中，由于这两个对象都是new出来的，所以hashCode是不一样的！
 * 		因此HashMap会将user1，user2存储成两个key，这个就不正确了。
 * 		所以对User方法重写hashCode方法和equals方法。
 * 	 
 * 	 HashMap中底层实现原理（关于HashCode和equals的）
 * 		1. 首先有一个table保存HashMap中所有key的hashCode值
 * 		2. 当调用put方法的时候，先获取key的hashCode值，并判断该值是否在table中存在，如果不存在那么就直接存进去
 * 			如果存在，然后调用key的equals方法与新的元素进行比较，相同的修改，不相同使用（链表+红黑树）进行存储，
 * 			这样的话，使用equals的次数会大大降低，效率会提升
 * 	 HashMap的数据结构
 * 		1. HashMap中存储了一个数组，该数组中每个元素是一个桶
 * 		2. 每个桶中存储的是HashCode相同的元素，采用链表的方式存储（jdk1.8之后：如果链表的长度大于8，那么使用红黑树存储）
 * 
 * 	 重写HashCode和equals方法必须满足如下条件：
 * 		1. 调用equals方法得到的结果为true，则两个对象的HashCode必须相等
 * 		2. 调用equals方法得到的结果为false，则连个对象的HashCode不一定相等
 * 		3. 两个对象的hashCode不相等，则调用equals方法得到的结果必须为false
 * 		4. 两个对象的hashCode相等，则调用equals方法得到的结果不一定相等
 * 
 * @author tiantao
 */
public class ReWriteHashCodeAndEquals {
	public static void main(String[] args) {
		User user1 = new User("小明", "15");
		User user2 = new User("小明", "15");
		UserReWrite userW1 = new UserReWrite("小明", "15");
		UserReWrite userW2 = new UserReWrite("小明", "15");
		System.out.println("判断两个没有重写hashCode和equals的user对象是否相等："+user1.equals(user2));
		System.out.println("判断两个有重写hashCode和equals的user对象是否相等："+userW1.equals(userW2));
		System.out.println();
		
		//没有重写hashCode和equals方法
		Map<User,String> mapUser = new HashMap<User, String>();
		mapUser.put(user1, "aaaa");
		mapUser.put(user2, "aaaa");
		System.out.println("将两个没有重写hashCode和equals的user对象放到HashMap中："+mapUser);
		
		//重写了hashCode和equals方法
		Map<UserReWrite,String> mapUserReWrite= new HashMap<UserReWrite, String>();
		mapUserReWrite.put(userW1, "aaaa");
		mapUserReWrite.put(userW2, "bbbb");
		System.out.println("将两个有重写hashCode和equals的user对象放到HashMap中："+mapUserReWrite);
	}
}

//没有重写equals和HashMap的实体类
class User{
	private String name;
	private String age;
	public User(String name, String age) {
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
}

//重写了equals和HashMap的实体类
class UserReWrite{
	private String name;
	private String age;
	public UserReWrite(String name, String age) {
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	@Override
	public int hashCode() {
		int hc = 0;
		if(name != null){
			hc = hc + name.hashCode();
		}
		if(age != null){
			hc = hc + age.hashCode();
		}
		return hc;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}else if(!(obj instanceof UserReWrite)){  
            return false;
        }  
		UserReWrite user = (UserReWrite) obj;
		Boolean isPass = true;
		//校验name是否相等
		if(this.getName() == user.getName()){		//如果两个指针地址相同，那么name的校验通过
			//校验通过
		}else if(this.getName() != null && this.getName().equals(user.getName())){	//如果name不为null，那么判断里面的值是否相同，如果相同那么通过
			//校验通过
		}else{
			isPass = false;			//校验失败
		}
		
		//校验age是否相等
		if(this.getAge() == user.getAge()){		//如果两个指针地址相同，那么age的校验通过
			//校验通过
		}else if(this.getAge() != null && this.getAge().equals(user.getAge())){	//如果age不为null，那么判断里面的值是否相同，如果相同那么通过
			//校验通过
		}else{
			isPass = false;			//校验失败
		}
		
		return isPass;
	}
	
	
}



















