package com.tiantao.common.utils;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 针对日期的操作
 * 		一个线程，一个template只会创建一个SimpleDateFormat对象
 * @author tiantao 
 * @version 1.0
 */
public class DateUtils {
	//一个线程，一个template只会创建一个SimpleDateFormat对象
		private static ThreadLocal<Map<String,SimpleDateFormat>> sdfs;
		//用于日志的打印
		private static Logger logger = LoggerFactory.getLogger(DateUtils.class);
		
		static{
			sdfs = new ThreadLocal<Map<String,SimpleDateFormat>>();
		}
		
		/**
		 * 获取格式化后的字符串
		 * @param dateTime  时间戳
		 * @param template	格式化后的模板
		 * @return
		 */
		public static String getDate(Long dateTime,String template){
			SimpleDateFormat sdf=new SimpleDateFormat(template);
			String format = sdf.format(dateTime);
			return format;
		}
		
		/**
		 * 将指定格式的字符串转换成时间
		 * @param dateStr	指定格式的字符串
		 * @param template	指定的格式
		 * @return
		 */
		public static Date getDate(String dateStr,String template){
			try {
				//1.如果为空，直接返回空
				if(StringUtils.isBlank(dateStr)){
					return null;
				}
				//2.如果不为空，转换后返回
				SimpleDateFormat sdf=new SimpleDateFormat(template);
				return sdf.parse(dateStr);
			} catch (ParseException e) {
				logger.error("date transition error, dateStr:"+dateStr+", template"+template);
				e.printStackTrace();
				return null;
			}
		}
		
		/**
		 * 获取指定两个时间之间的间隔天数
		 * @param date1
		 * @param date2
		 * @return 返回间隔天数
		 */
		public static Integer getIntervalDayNumber(Date date1,Date date2){
			if(date1 == null){
				date1 = new Date();
			}
			if(date2 == null){
				date2 = new Date();
			}
			Long intervalTime = date1.getTime() - date2.getTime();
			return (int)(intervalTime / (1000*60*60*24));
		}
		
		/**
		 * 获取指定两个时间之间的间隔月数
		 * @param date1
		 * @param date2
		 * @return 返回间隔天数
		 */
		public static Integer getIntervalMonthNumber(Date date1,Date date2){
			if(date1 == null){
				date1 = new Date();
			}
			if(date2 == null){
				date2 = new Date();
			}
			
			Calendar c1=Calendar.getInstance();
			c1.setTime(date1);
		    Calendar c2=Calendar.getInstance();
		    c2.setTime(date2);
		    
		    //1. 计算间隔年数
		    int year =c2.get(Calendar.YEAR)-c1.get(Calendar.YEAR);
			//2. 计算间隔月数
		    return year*12 + c2.get(Calendar.MONTH)-c1.get(Calendar.MONTH);
		}
		
		/**
		 * 获取今天timeStr时间的日期对象
		 * @param timeStr 表示当天的时间（只有时分秒，格式：HH:mm:ss  eg:15:30:00）
		 * @return
		 */
		public static Date getCurrDate(String timeHms){
			//1.获取当前时间的年月日
			String dateYMD = getDate(System.currentTimeMillis(), "yyyy-MM-dd");
			
			//2. 拼接时间字符串
			String time = dateYMD +" "+ timeHms;
			
			//3. 将拼接好的字符串，转换成时间
			return getDate(time, "yyyy-MM-dd HH:mm:ss");
		}
		
		/**
		 * 获取指定日期的n天后，或者那天前的日期
		 * @param date	时间
		 * @param day	如果要获取那天后的数据，那么n为正数，如果要获取n天前的数据，那么n为负数
		 * @return
		 */
		public static Date getNDayAfterDate(Date date,Integer n){
			//1. 创建Calendar实例对象，并将时间加载进去
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			
			//2. 添加天数
			cal.add(Calendar.DATE, n);
			
			//3. 将最后的结果返回
			return cal.getTime();
		}		
		
		/**
		 * 通过模板，获取SimpleDateFormat对象信息
		 * @param template	模板
		 * @return
		 */
		public static SimpleDateFormat getSdf(String template){
			//1. 获取当前线程下的所有模板信息
			Map<String, SimpleDateFormat> sdfsLocalMap = sdfs.get();
			
			//2. 如果该模板已存在，那么直接返回
			if(sdfsLocalMap.containsKey(template)){
				return sdfsLocalMap.get(template);
			}
			
			//3. 如果该模板不存在，那么创建该模板，并保存到该线程中
			SimpleDateFormat sdf = new SimpleDateFormat(template);
			sdfs.get().put(template, sdf);	//将该模板保存到线程中
			return sdf;
		}
}
